var Congress = artifacts.require("./Congress.sol");
var Contract = artifacts.require("./Contract.sol");
var Token = artifacts.require("./Sarafu.sol");

module.exports = function(deployer) {
  deployer.deploy(Congress);
  deployer.deploy(Contract);
  deployer.deploy(Token);
};